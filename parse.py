#!/usr/bin/python

# Add Jan Robert bij T8
# Change Niels (J1) bij T9
# Change Ferry (J1) bij T9

# Create hash table with add and replace names
# [T1] = Arno 
# [TX] = Paul Meeteren wordt Paul M.
# [TX] = Ferry wordt Ferry (J1)

import re
import sys
import os

# Usage message
help = "Usage: ./" + os.path.basename(__file__) + ' [options] ' + \
    '\n  <category>\tSpecifieer de categorie: jeugd, senioren of duo' + \
    '\n  <action>\tSpecificeer de output: html of excel'

#if len(sys.argv[1]) == 0:
#	sys.exit()

category = sys.argv[1]
action = sys.argv[2]
team = ""
names = ("Meeteren","Jansen")

x = {}
y = {}

with open("resultaat_"+category,"r") as f:
    for line in f:
        if 'TSTZ' in line:
            res = re.split(r'\s{2,}', line)
            if team != res[2]:
                team = res[2]
            a =  [re.sub('[()]', '', item) for item in re.split(r'\s', res[1])]
            if any(s in a for s in names):
                fullname = a[-1] + " " + a[len(a)-2][0] + "."
            else:
                fullname = a[-1]
            x.setdefault(team,[]).append([fullname,res[3],res[4],res[5]])

with open("standen_"+category,"r") as f:
    for line in f:
        if 'klasse' in line:
            klasse = line.strip()
            if y.has_key('key'):
                y[team] = y.pop('key')
        res = re.split(r'\s{3,}', line.strip())
        if 'TSTZ' in res[0]:
            team = res[0]
        if len(res) == 2:
            y.setdefault('key',[]).append([klasse,res[0],res[1]])
    y[team] = y.pop('key')

if action == 'excel':
    for k in sorted(y):
        r = list(enumerate(y[k]))
        t = list(enumerate(x[k]))
        c = 0
        print r[c][1][0],"\t\t",k
        while c < len(r):
            if c < len(t):
                print r[c][1][1],"\t",r[c][1][2],"\t",t[c][1][0],"\t",t[c][1][1]," ",t[c][1][2]," ",t[c][1][3]
            else:
                print r[c][1][1],"\t",r[c][1][2]
            c += 1
        print "\r"

if action == 'html':
    print '<table border="0" width="500" cellspacing="0" cellpadding="0"><colgroup><col width="180"/> <col width="70"/> <col width="125"/><col width="40"/> <col width="40"/> <col width="45"/></colgroup>'
    for i,k in enumerate(sorted(y)):
        r = list(enumerate(y[k]))
        t = list(enumerate(x[k]))
        c = 0
        print '<tr><td style="text-align: left;" colspan="2">'+r[c][1][0]+'</td><td style="text-align: left;" colspan="4">'+k+'</td></tr>'
        while c < len(r):
            if c < len(t):
                print '<tr><td style="text-align: left;">'+r[c][1][1]+'</td><td style="text-align: left;">'+r[c][1][2]+'</td><td style="text-align: left;">'+t[c][1][0]+'</td><td style="text-align: left;">'+t[c][1][1]+'</td><td style="text-align: left;">'+t[c][1][2]+'</td><td style="text-align: left;">'+t[c][1][3]+'</td></tr>'
            else:
                print '<tr><td style="text-align: left;">'+r[c][1][1]+'</td><td style="text-align: left;">'+r[c][1][2]+'</td><td style="text-align: left;"></td><td style="text-align: left;"></td><td style="text-align: left;"></td><td style="text-align: left;"></td></tr>'
            c += 1
        if i != len(y)-1:
            print '<tr><td style="text-align: left;"></td><td style="text-align: left;"></td><td style="text-align: left;"></td><td style="text-align: left;"></td><td style="text-align: left;"></td><td style="text-align: left;"></td></tr>'
    print '</tbody></table>'
